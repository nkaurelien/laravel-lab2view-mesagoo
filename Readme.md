

# LARAVEL LAB2VIEW MESAGOO


![Packagist License](https://img.shields.io/packagist/l/nkaurelien/laravel-lab2view-mesagoo)
![Packagist Version (including pre-releases)](https://img.shields.io/packagist/v/nkaurelien/laravel-lab2view-mesagoo?include_prereleases)
![Packagist Downloads](https://img.shields.io/packagist/dt/nkaurelien/laravel-lab2view-mesagoo)

## Description

Mesagoo is a fast saas to send message from any where to any where. <br>
Made in cameroun.<br>


## Installation


```shellscript
composer require nkaurelien/laravel-lab2view-mesagoo
```

(optional) Add the service provider in `config\app.php`

```php 
    'providers' => [
        #...
        \Nkaurelien\Mesagoo\Providers\MesagooServiceProvider::class,
    ]
```
(optional) Add the service facade in `config\app.php`
```php 
    'aliases' => [
        #...
        'Mesagoo' => \Nkaurelien\Mesagoo\Facades\Mesagoo::class
    ]
```

## Configuration

Add config to `config/services.php`

```text

        'mesagoo' => [
            'token' => env('MESAGOO_TOKEN'),
            'sender_code' => env('MESAGOO_SENDER_CODE'),
            'gateway' => env('MESAGOO_GATEWAY', 'premium'),
        ],

```


Don't forget to cache the configurations again with the command `php artisan config:cache` or `php artisan optimize`



## Usage
use the facade instead of injection then do:
```php
    use Nkaurelien\Mesagoo\Facades\Mesagoo;
    #
    $number='6999999';
    $country_code='+237';
    $message='Hello';
    $mesagooResponse = Mesagoo::sendSingleTextMessage($number, $message, $country_code);
```


## Notification
send a notification with the custom mesagoo channel:

```shellscript
php artisan make:notification MesagooTestNotification
```

activate the channel
```php

    public function via($notifiable)
    {
        return [
            MesagooNotificationChannel::class, // new line
            'mail',
        ];
    }
```
add The channel method
```php
       public function toMesagooSMS($notifiable)
       {
            return [
                'type' => 'text',
                'phone' => $notifiable->phoneNumber,
                'message' => 'Thank you',
                'country_code' => $notifiable->country->iso,
            ];
       }  
```
then run it
```php 
    // send your mail and SMS notifications
   $user->notify(new MesagooTestNotification);
   
    // or send only a SMS via mesagoo notification channel
   $user->notifyNow(new MesagooTestNotification, [MesagooNotificationChannel::class]);
```
## Capture events
You can listen to : <br>
- **Nkaurelien\Mesagoo\Events\MesagooSuccessful** is fired after the message was successfully sent 

## Use commands
````shell script
    php artisan mesagoo:sendsms {phone_number} {message} # to send a direct message
````

## Todo
- [ ] Add  more exception class and Event

## Useful links
- [Create Your Account on Mesagoo](https://www.mesagoo.com/register/)


if your have a suggestion , write me to nkaurelien@gmail.com