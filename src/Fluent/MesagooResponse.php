<?php


namespace Nkaurelien\Mesagoo\Fluent;


use Illuminate\Support\Fluent;

/**
 * Class MomoToken
 * @package  Nkaurelien\Momopay\Fluent
 * @property boolean success If reauest is successful and has not empty body
 * @property string message The mesagoo's server message
 * @property mixed data The json parsed response body from mesagoo's server
 * @property mixed raw The raw response body from mesagoo's server
 */
class MesagooResponse extends Fluent
{

    const STATUS_SUCCESSFUL = 'M200';

    public function isSuccessful()
    {
        if (!$this->data) {
            return false;
        }
        if (is_array($this->data)) {
            return $this->data['code'] === self::STATUS_SUCCESSFUL;
        }
        return $this->data->code === self::STATUS_SUCCESSFUL;
    }


}
