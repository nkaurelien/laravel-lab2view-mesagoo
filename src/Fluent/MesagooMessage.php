<?php


namespace Nkaurelien\Mesagoo\Fluent;


use Illuminate\Support\Fluent;

/**
 * Class MomoToken
 * @package  Nkaurelien\Momopay\Fluent
 * @property string phoneNumber The Phone number with the country iso code. For example +237XXXXXXXX
 * @property string message The text message to send
 * @property string type Type of message, Default is 'text', possible values are text
 */
class MesagooMessage extends Fluent
{

}
