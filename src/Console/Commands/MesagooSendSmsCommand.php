<?php

namespace Nkaurelien\Mesagoo\Console\Commands;

use Illuminate\Console\Command;
use Nkaurelien\Mesagoo\Helpers\MesagooHelper;
use Throwable;


class MesagooSendSmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan mesagoo:sendsms 237654XXXX74 Hello
     *
     * @var string
     */
    protected $signature = 'mesagoo:sendsms {phone_number} {message}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoyer un simple message par SMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        // $options = $this->options();

        $phoneNumber = $this->argument('phone_number');

        $testmsg = sprintf("%s , send to %s at %s", 'TEST MESSAGE FORM MESSAGE', $phoneNumber, now()->toDateTimeString());

        $msg = $this->argument('message') ?? $testmsg;


        if (empty($phoneNumber)) {
            $this->error('Give a Phone number to send test message. CMD: ' . $this->signature);
        }

        try {
            MesagooHelper::sendSingleMessage($phoneNumber, $msg, $countrycode = '');
            $this->info(sprintf("Great! Successfully send  to %s at %s", $countrycode . '' . $phoneNumber, now()->toDateTimeString()));
        } catch (Throwable $exception) {
            $this->error('Sorry! Please try again latter :: Error message is ' . $exception->getMessage());
        }
    }
}
