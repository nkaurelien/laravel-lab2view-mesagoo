<?php

namespace Nkaurelien\Mesagoo\Helpers;

use Httpful\Mime;
use Httpful\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;
use Nkaurelien\Mesagoo\Exceptions\MesagooException;
use Nkaurelien\Mesagoo\Fluent\MesagooResponse;
use Throwable;

class MesagooHelper
{

    /**
     * @param $number
     * @param $message
     * @param $country_code
     * @param string $type
     * @return MesagooResponse
     * @throws Throwable
     */
    public static function sendSingleMessage($number, $message, $country_code, $type = 'text')
    {


        $token = Config::get('services.mesagoo.token');
        $sender_code = Config::get('services.mesagoo.sender_code');
        $gateway = Config::get('services.mesagoo.gateway', 'premium');

        $phone = $country_code . '' . $number;
        $phone = ltrim($phone, '+');
        $phone = ltrim($phone, '00');

        $body = JSON::encode([
            'token' => '' . $token,
            'sender_code' => '' . $sender_code,
            'phone' => $phone,
            'message' => $message,
            'type' => '' . $type,
            'gateway' => $gateway
        ]);
        $headers = array(
            'Accept' => 'application/json',
//            'Content-Type' => 'application/x-www-form-urlencoded',
        );
        $url = "https://mesagoo.com/api/sends/single";
        try {

            $response = Request::post($url, $body, Mime::JSON)
                ->withoutStrictSsl()
                ->addHeaders($headers)
                ->send();

            if ($response->code > 500 || ($response->hasBody() && $response->body->code === 'M500')) {
                throw new MesagooException($response->raw_body);
            }


            $data = [
                'success' => isset($response->code) && $response->hasBody(),
//                'success' => isset($response->code) && $response->hasBody() && $response->body->code == 'M200',
                'message' => $response->body->message,
                'data' => [
                    'message_id' => $response->body->message_id,
                    'code' => $response->body->code,
                ],

                'raw' => $response->raw_body,
            ];

            $response = new MesagooResponse;
            $response->success = $data['success'];
            $response->message = $data['message'];
            $response->raw = $data['raw'];
            $response->data = $data['data'];

            return $response;

        } catch (Throwable $e) {
            Log::error('MESAGOO ERROR::' . $e->getMessage(), [
                'FILE' => $e->getFile(),
                'LINE' => $e->getLine()
            ]);

            throw $e;
        }
    }
}
