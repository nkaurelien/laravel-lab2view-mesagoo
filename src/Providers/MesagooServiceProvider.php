<?php

namespace Nkaurelien\Mesagoo\Providers;

use Illuminate\Support\ServiceProvider;
use Nkaurelien\Mesagoo\Console\Commands\MesagooSendSmsCommand;
use Nkaurelien\Mesagoo\Facades\Mesagoo;
use Nkaurelien\Mesagoo\Repository\MesagooRepository;

class MesagooServiceProvider extends ServiceProvider
{


    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {


        $this->app->bind(Mesagoo::class, function () {
            return new MesagooRepository;
        });

        $this->registerArtisanCommand();

    }

    /**
     * Register commands.
     *
     * @return void
     */
    public function registerArtisanCommand()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                MesagooSendSmsCommand::class,
            ]);
        }
    }


}
