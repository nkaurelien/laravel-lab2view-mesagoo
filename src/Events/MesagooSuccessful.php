<?php

namespace Nkaurelien\Mesagoo\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Nkaurelien\Mesagoo\Fluent\MesagooResponse;

class MesagooSuccessful
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var array
     */
    public array $responseData;

    /**
     * Create a new event instance.
     *
     * @param MesagooResponse $responseData
     */
    public function __construct(MesagooResponse $responseData)
    {

        $this->responseData = $responseData->toArray();
    }

//    /**
//     * Get the channels the event should broadcast on.
//     *
//     * @return \Illuminate\Broadcasting\Channel|array
//     */
//    public function broadcastOn()
//    {
//        return new PrivateChannel('mesagoo');
//    }
}
