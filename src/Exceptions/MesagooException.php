<?php


namespace Nkaurelien\Mesagoo\Exceptions;


use Exception;
use Throwable;

class MesagooException extends Exception
{

    protected $message = 'Mesagoo Unhandled Exception';

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message ?? $this->message, $code, $previous);
    }
}