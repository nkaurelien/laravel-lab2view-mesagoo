<?php
/**
 * Created by PhpStorm.
 * User: nkaurelien
 * Date: 27/02/19
 * Time: 10:13
 */

namespace Nkaurelien\Mesagoo\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;
use Nkaurelien\Mesagoo\Helpers\MesagooHelper;
use Throwable;

class MesagooNotificationChannel
{


    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {

        $data = $notification->toMesagooSMS($notifiable);

//        dd($data);
//        if ($data instanceof SMS) {
//            $data = $data->toArray();
//        }

        try {
            MesagooHelper::sendSingleMessage($data['phone'], $data['message'], $data['country_code'], $data['type']);
        } catch (Throwable $e) {
            Log::error($e->getMessage(), [
                'FILE' => $e->getFile(),
                'LINE' => $e->getLine()
            ]);
        }
    }


}
