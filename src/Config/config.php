<?php

return [

    'mesagoo' => [
        'token' => env('MESAGOO_TOKEN'),
        'sender_code' => env('MESAGOO_SENDER_CODE'),
        'gateway' => env('MESAGOO_GATEWAY', 'premium'),
    ],

];

