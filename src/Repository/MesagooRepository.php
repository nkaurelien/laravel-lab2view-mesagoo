<?php


namespace Nkaurelien\Mesagoo\Repository;


use Nkaurelien\Mesagoo\Events\MesagooSuccessful;
use Nkaurelien\Mesagoo\Fluent\MesagooResponse;
use Nkaurelien\Mesagoo\Helpers\MesagooHelper;
use Throwable;

class MesagooRepository
{

    /**
     * @param $number
     * @param $message
     * @param string $country_code
     * @return MesagooResponse
     * @throws Throwable
     */
    public function sendSingleTextMessage($number, $message, $country_code = '')
    {
        $response = MesagooHelper::sendSingleMessage($number, $message, $country_code, $type = 'text');


        if ($response->success) {
            event(new MesagooSuccessful($response));
        }

        return $response;
    }


}
