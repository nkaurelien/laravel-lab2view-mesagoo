<?php


namespace Nkaurelien\Mesagoo\Facades;

use Illuminate\Support\Facades\Facade;
use Nkaurelien\Mesagoo\Fluent\MesagooResponse;

/**
 * Class Mesagoo
 * @package Nkaurelien\Mesagoo\Facades
 * @method static MesagooResponse sendSingleTextMessage(string $number, string $message, string $country_code = '') Send a single SMS
 */
class Mesagoo extends Facade
{
    protected static function getFacadeAccessor()
    {
        return self::class;
    }

}
